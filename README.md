
## Project for deploy ELK-cluster to Docker swarm. 

Need 3 nodes with docker swarm for deploy.  

Conteyners:
 - haproxy - 1
 - kibana - 1
 - logstash - 1
 - elasticsearch - 3

Add line to /etc/sysctl.conf:  
> vm.max_map_count=262144

Restart sysctl:  
> sysctl -p

Make Label to nodes with persistent volume for elasticsearch:     
> sudo docker node update --label-add elasticsearch(number)=true (node)  

example:  
> sudo docker node update --label-add elasticsearch01=true vm1.lessons

Make folders in hosts/nodes for save DB elasticsearch  
example:   
> mkdir /elasticsearch01 && chmod 777 /elasticsearch01 

